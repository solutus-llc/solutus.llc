---
title: 'Home'
intro_image: images/undraw_problem_solving_ft81.svg
intro_image_absolute: true # edit /assets/scss/components/_intro-image.scss for full control
intro_image_hide_on_mobile: true
---

# SOLUTUS

## 事業内容
* WEBサイト制作
* WEBサイト導入・運用支援
* 持続的WeB開発
* マイニングファーム構築・運用
* 中小規模インフラ構築・運用(機材選定・物理設置〜サーバ構築・運用)
* 小規模システム開発
## 問合せ先
←または↑のメールアドレスまで
## 取引先
* ワサビ株式会社
* 株式会社アスカデザイン
* 株式会社ビープライス
* 株式会社モノラポ
* 株式会社フィエスタプロジェクト
* 株式会社まちづくりプラットフォーム
* その他、日本全国多数
## 設立
2017年2月22日

## 会社情報

会社名: 无合同会社 / 英表記: SOLUTUS,LLC
